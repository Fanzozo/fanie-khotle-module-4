import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import './feature.dart';

void main() {
  runApp(const Dashboard());
}

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          brightness: Brightness.light,
          primarySwatch: Colors.orange,
        ).copyWith(
          secondary: Colors.black,
        ),
        textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.black)),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              primary: Colors.orange, // background color
              textStyle: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
        ),
      ),
      title: 'Dashboard',
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(60.0), // here the desired height
          child: AppBar(title: const Text('Dashboard') // ...
              ),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  child: const Icon(
                    Icons.person,
                    color: Colors.white10,
                    size: 100.0,
                    textDirection: TextDirection.ltr,
                    semanticLabel: 'Icon',
                  ),
                  alignment: Alignment.topCenter,
                  padding: const EdgeInsets.all(8.0),
                  decoration: BoxDecoration(
                    //border: Border.all(),
                    borderRadius: BorderRadius.circular(100),
                    color: Colors.black45,
                  ),
                  width: 120,
                  height: 120,
                ),
                const SizedBox(height: 50),
                const Text('John Doe', style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal)),
                const SizedBox(height: 20),
                Builder(
                  builder: (context) => ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Feature()),
                      );
                    },
                    child: const Text(
                      'Explore',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Builder(
                  builder: (context) => ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Profile()),
                      );
                    },
                    child: const Text(
                      'edit profile',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Home()),
            );
          },
          backgroundColor: Colors.green,
          child: const Icon(Icons.navigation),
        ),
      ),
    );
  }
}

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          brightness: Brightness.light,
          primarySwatch: Colors.orange,
        ).copyWith(
          secondary: Colors.black,
        ),
        textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.black)),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              primary: Colors.orange, // background color
              textStyle: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
        ),
      ),
      title: 'Profile',
      home: Scaffold(
        appBar: PreferredSize(
            preferredSize: const Size.fromHeight(60.0), // here the desired height
            child: AppBar(title: const Text('Edit profile') // ...
                )),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  child: const Icon(
                    Icons.person,
                    color: Colors.white10,
                    size: 100.0,
                    textDirection: TextDirection.ltr,
                    semanticLabel: 'Icon',
                  ),
                  alignment: Alignment.topCenter,
                  padding: const EdgeInsets.all(8.0),
                  decoration: BoxDecoration(
                    //border: Border.all(),
                    borderRadius: BorderRadius.circular(100),
                    color: Colors.black45,
                  ),
                  width: 120,
                  height: 120,
                ),
                const SizedBox(height: 50),
                const Text('Edit profile', style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal)),
                const SizedBox(height: 20),
                TextFormField(
                  decoration: const InputDecoration(
                    //icon: Icon(Icons.person),

                    hintText: 'Change name',
                    labelText: 'Name *',
                    icon: Icon(Icons.mode_edit),
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                  validator: (String? value) {
                    return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
                  },
                ),
                const SizedBox(height: 20),
                TextFormField(
                  decoration: const InputDecoration(
                    //icon: Icon(Icons.email),
                    hintText: 'Change email',
                    labelText: 'Email *',
                    icon: Icon(Icons.mode_edit),
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                  validator: (String? value) {
                    return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
                  },
                ),
                const SizedBox(height: 20),
                TextFormField(
                  decoration: const InputDecoration(
                    //icon: Icon(Icons.password),
                    hintText: 'Change password',
                    labelText: 'Password *',
                    icon: Icon(Icons.mode_edit),
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                  validator: (String? value) {
                    return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
                  },
                ),
                const SizedBox(height: 20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(width: 100),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        'Save',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
